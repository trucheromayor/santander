var map, geocoder, mitoken;

      require([
      "dojo/_base/Color",
      "esri/map", "esri/dijit/Geocoder", "dojo/domReady!", "esri/request",
      "esri/symbols/SimpleMarkerSymbol",
      "esri/symbols/SimpleLineSymbol",
      "esri/symbols/SimpleFillSymbol",
      "esri/graphic",
      "esri/graphicsUtils",
      "esri/geometry/webMercatorUtils"
      ], function(Color, Map, Geocoder, esriRequest, SimpleMarkerSymbol, SimpleLineSymbol, SimpleFillSymbol, Graphic, graphicsUtils, webMercatorUtils) {

        map = new Map("map",{
          basemap: "streets",
          center: [-3, 41],
          zoom: 7 
        });

        ////Generamos un token

        generarToken();

        function generarToken(){

          var parametrosToken = {
              username: "trucheromayor",
              password: "martin33",
              referer: "http://www.jongarrido.es",

          }

          var requestComplete = "https://www.arcgis.com/sharing/generateToken?"+
            "username="+parametrosToken.username+
            "&password=" + parametrosToken.password +
            "&referer=" + parametrosToken.referer+
            "&expiration=15&f=json";

           var requestToken = esri.request({
                    url: requestComplete ,
                    handleAs: "json"
           });

          function requestSucceeded(data) {
            console.log("Generado token"); // print the data to browser's console
            mitoken = data.token;

          }

          function requestFailed(error) {
            console.log("Error: ", error.message);
          }

          requestToken.then(requestSucceeded, requestFailed)

        }


        // Geocoder Widget

        geocoder = new Geocoder({ 
          map: map,
          token: mitoken,
          arcgisGeocoder: {
              placeholder: "Buscar dirección"
          },


          f: "json"
        }, "search");
        geocoder.startup();

        //// tema service areas

        dojo.connect(map, "onClick", function (evt){
          var distancia = document.getElementById("distancia").value;
          var posicion = evt;
          //var posicion = webMercatorUtils.webMercatorToGeographic(evt.mapPoint);
          serviceAreas(distancia, posicion);

         });

         function serviceAreas(distancia, posicion){

          console.log("Calculando service areas a distancia:" + distancia + " y posicion:" + posicion.mapPoint.x + ", " + posicion.mapPoint.y);
          console.log("Con tonken:" + mitoken);

          dibujarPuntoInicial(posicion);

          var pointJson = '{'+
                        '"features": ['+
                        '{'+
                            '"geometry": {'+
                                '"x": '+ posicion.mapPoint.x + ','+
                                '"y": ' + posicion.mapPoint.y + ','+
                                '"spatialReference": {'+
                                '"wkid": 102100'+ 
                                '}'+
                            '},'+
                            '"attributes": {'+
                            '"Posicion": "mi posicion" '+                         
                            '}'+
                        '}'+
                        
                    '] '+
                '}';

          var puntoLatLong = esri.geometry.webMercatorToGeographic(posicion.mapPoint);

                         
          var serviceareasRequest = esri.request({
            url: "http://route.arcgis.com/arcgis/rest/services/World/ServiceAreas/NAServer/ServiceArea_World/solveServiceArea?token=" + mitoken +
              "&facilities="+ puntoLatLong.x + "," + puntoLatLong.y +
              "&outSR=102100&f=json&defaultBreaks=["+distancia+"]",
               handleAs: "json"
          }); 

          console.log("http://route.arcgis.com/arcgis/rest/services/World/ServiceAreas/NAServer/ServiceArea_World/solveServiceArea?token=" + mitoken +
              "&facilities="+ puntoLatLong.x + "," + puntoLatLong.y +
             "&outSR=102100&f=json&defaultBreaks="+distancia+"]");

          function serviceareasRequestSucceeded (data){
          console.log("Drive Time calculado...");
          dibujarPoligonos(data);
          
         }
         function serviceareasRequestFailed(data){
          console.log("Error: " + data);
         }

         serviceareasRequest.then(serviceareasRequestSucceeded, serviceareasRequestFailed);
         
         }

         
        function dibujarPuntoInicial(punto){

        console.log("Dibujando punto inicial");
        clickpoint = punto;
        map.graphics.clear(); //clear existing graphics    
        //define the symbology used to display the results and input point
        var pointSymbol = new esri.symbol.SimpleMarkerSymbol(
          esri.symbol.SimpleMarkerSymbol.STYLE_DIAMOND, 
          20,
          new esri.symbol.SimpleLineSymbol(
            esri.symbol.SimpleLineSymbol.STYLE_SOLID,
            new dojo.Color([88,116,152]), 2
          ),
          new dojo.Color([88,116,152,0.45])
        );
        var inPoint = new esri.geometry.Point(punto.mapPoint.x,punto.mapPoint.y,map.spatialReference);
        var location = new esri.Graphic(inPoint,pointSymbol);

        map.graphics.add(location);

         }
         



      function dibujarPoligonos(salida) {

        map.graphics.clear();

        console.log("Dibujando poligono...");

        console.log(salida.saPolygons.features[0]);

        var g = new esri.Graphic(salida.saPolygons.features[0]);
        cosole.log(g);

        map.graphics.add(g);

        // map.graphics.clear();
  //       var symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
  //   new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOT,
  //   new Color([255,0,0]), 2),new Color([255,255,0,0.25])
  // );
  //       var sym = new esri.symbol.SimpleFillSymbol({"color":[255,255,0,64],"outline":{"color":[255,0,0,255],"width":1.5,"type":"esriSLS","style":"esriSLSDashDot"},"type":"esriSFS","style":"esriSFSSolid"});
  //     console.log("a recorrer....")

  //     var geometries = salida.saPolygons.features;

  //     dojo.forEach(geometries, function(element) {

  //       var graphic = new esri.Graphic().setSymbol(sym).setGeometry(element.geometry).setAttributes(element.attributes);
        
  //       console.log(graphic);
  //       // map.graphics.add(graphic);


  //     });
  //     console.log("al zoooooom");

  //     map.setExtent(graphicsUtils.graphicsExtent(map.graphics.graphics), true);
    }




      });
      
    